
package main

import (
	"database/sql"
	"log"
	_ "github.com/mattn/go-sqlite3"  // Import go-sqlite3 library
)
func insertMessage(db *sql.DB, messagetext string, chat_id int64) error {
    log.Println("Inserting message record ...")
    insertMessagesSQL := `INSERT INTO Messages(chat_id, content) VALUES (?, ?)`
    statement, err := db.Prepare(insertMessagesSQL)
    if err != nil {
        log.Fatalf("Unable to prepare statement: %v", err)
		return err
    }
    defer statement.Close()

    _, err = statement.Exec(chat_id, messagetext)
    if err != nil {
        log.Fatalf("Unable to execute statement: %v", err)
		return err
    }
	return nil
}

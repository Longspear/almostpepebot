package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"database/sql"
	"log"
	_ "github.com/mattn/go-sqlite3"  // Import go-sqlite3 library
)

const tokenlocation = "./config/token.txt"

func main() {

	token, err := readBotToken(tokenlocation)
	if err != nil {
		log.Panicf("Token error: ", err)
	}
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)
	
	sqliteDatabase, _ := sql.Open("sqlite3", "./almostpepebot.db")  
    defer sqliteDatabase.Close()

	for update := range updates {

		if update.Message != nil {
			m := update.Message
			if m.From == nil {
				continue
			}
			log.Printf("Я получил сообщение")
			if m.Chat.Type == "supergroup" || m.Chat.Type == "group"{
				err := handleMessage(bot, m, sqliteDatabase)
				if err != nil {
					log.Printf("[%s] %s,   err: %s", update.Message.From.UserName, update.Message.Text, err.Error())
					continue
				}

				log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)
			}
		}
	}
}

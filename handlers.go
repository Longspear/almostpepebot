package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"database/sql"
	_ "github.com/mattn/go-sqlite3" // Import go-sqlite3 library
	"strings"
	"time"
	"math/rand"
	"log"
    "fmt"
)

var stopWords = map[string]struct{}{
	"the": {}, "be": {}, "to": {}, "of": {}, "and": {}, "a": {}, "in": {}, "that": {}, "have": {},
	"I": {}, "it": {}, "for": {}, "not": {}, "on": {}, "with": {}, "he": {}, "as": {}, "you": {},
	"do": {}, "at": {}, "this": {}, "but": {}, "his": {}, "by": {}, "from": {}, "they": {}, "we": {},
	"say": {}, "her": {}, "she": {}, "or": {}, "an": {}, "will": {}, "my": {}, "one": {}, "all": {},
	"would": {}, "there": {}, "their": {}, "what": {}, "so": {}, "up": {}, "out": {}, "if": {},
	"about": {}, "who": {}, "get": {}, "which": {}, "go": {}, "me": {}, "с": {}, "за": {}, "он": {}, "ты": {},
	"пепе": {}, "со": {}, "а": {}, "Пепе": {}, "же": {}, "к": {},
}

func handleMessage(bot *tgbotapi.BotAPI, message *tgbotapi.Message, db *sql.DB) error {
	receivedMessageText := strings.ToLower(message.Text)
	receivedMessageChatID := message.Chat.ID
	if receivedMessageText != "" {
		err := insertMessage(db, receivedMessageText, receivedMessageChatID)
		if err != nil {
			return err
		}

		phrase, err := getChatPhrase(db, receivedMessageChatID, receivedMessageText)
		if err != nil {
			return err
		}

		// Check if phrase contains any word from the received message
		msgWords := strings.Fields(receivedMessageText)
		phraseWords := strings.Fields(strings.ToLower(phrase))

		phraseWordsMap := make(map[string]struct{})
		for _, word := range phraseWords {
			phraseWordsMap[word] = struct{}{}
		}

		for _, word := range msgWords {
			if _, ok := phraseWordsMap[word]; ok {
				log.Printf("Word '%s' from the received message is present in the generated phrase", word)
				msg := tgbotapi.NewMessage(message.Chat.ID, phrase)
				if strings.ToLower(word) == "пепе" || strings.ToLower(word) == "Пепе" {
					bot.Send(msg)
				} else {
					rand.Seed(time.Now().UnixNano())
					sendornot := rand.Intn(100)
					if sendornot < 10 {
						bot.Send(msg)
					}
				}
				return nil
			} else {
				log.Printf("Word '%s' from the received message is not present in the generated phrase", word)
			}
		}
	}
	return nil
}


func getChatPhrase(db *sql.DB, chatID int64, receivedMessageText string) (string, error) {
    query := `SELECT content FROM Messages WHERE chat_id = ?`
    rows, err := db.Query(query, chatID)
    if err != nil {
        return "", err
    }
    defer rows.Close()

    var contents []string
    for rows.Next() {
        var content string
        if err := rows.Scan(&content); err != nil {
            return "", err
        }
        content = strings.ToLower(content)
        contents = append(contents, content)
    }

    allContents := strings.Join(contents, " ")
    words := strings.Fields(allContents)

    if len(words) <= 1 {
        return allContents, nil
    }

    // Create bigrams and a simple Markov chain model
    bigrams := make(map[string][]string)
    for i := 0; i < len(words)-1; i++ {
        key := words[i]
        if _, ok := bigrams[key]; ok {
            bigrams[key] = append(bigrams[key], words[i+1])
        } else {
            bigrams[key] = []string{words[i+1]}
        }
    }

    // Start with a random word from the received message
    msgWords := strings.Fields(receivedMessageText)
    if len(msgWords) == 0 {
        return "", fmt.Errorf("Received message is empty")
    }
    rand.Seed(time.Now().UnixNano())
    start := msgWords[rand.Intn(len(msgWords))]

    // Generate the rest of the phrase by following the bigram approach
    phrase := []string{start}
    for {
        if len(phrase) >= 1 {
            lastWord := phrase[len(phrase)-1]
            if nexts, ok := bigrams[lastWord]; ok {
                next := ""
                rand.Seed(time.Now().UnixNano())
                if len(nexts) > 0 {
                    next = nexts[rand.Intn(len(nexts))]
                } else {
                    break
                }

                // Add randomness: 20% of the time, use a random word instead
                if rand.Intn(100) < 20 {
                    randomWord, err := getRandomWord(db)
                    if err != nil {
                        log.Printf("Could not get a random word: %v", err)
                    } else {
                        next = randomWord
                    }
                }

                phrase = append(phrase, next)
                if len(phrase) >= rand.Intn(15) + 3 {
                    break
                }
            } else {
                break
            }
        }
    }

    return strings.Join(phrase, " "), nil
}

func getRandomWord(db *sql.DB) (string, error) {
    query := `SELECT content FROM Messages`
    rows, err := db.Query(query)
    if err != nil {
        return "", err
    }
    defer rows.Close()

    var words []string
    for rows.Next() {
        var content string
        if err := rows.Scan(&content); err != nil {
            return "", err
        }
        content = strings.ToLower(content)
        words = append(words, strings.Fields(content)...)
    }

    if len(words) == 0 {
        return "", fmt.Errorf("No words in database")
    }

    rand.Seed(time.Now().UnixNano())
    return words[rand.Intn(len(words))], nil
}
